const express = require("express");

const app = express();
const port = process.env.PORT || 3000;

app.get("/", function(req, res){
    res.json({status: "OK", message: "Welcome to Scrapy Data Service"});
});

const router = require("./api/routes/ScrapyRoute");
app.use("/api", router);

app.listen(port, () => console.log("Server is started."));

