const express = require("express");
const router = express.Router();

const ScrapyController = require('../controllers/ScrapyController');

router.use("*", function(req, res, next){
    if(req.query.access_key !== "aLOxxqzwPe71mYbPAKKV") {
        res.sendStatus(403);
    } else {
        next();
    }
});

router.get("/:spiderName", function(req, res) {
    ScrapyController.query(req.params, req.query, results => {
        res.json(results);
    });
});

module.exports = router;