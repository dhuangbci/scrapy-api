const { Pool } = require('pg');

const pool = new Pool({
    connectionString: process.env.DATABASE_URL
});

module.exports = {
    query: (params, query, callback) => {
        const start = query.start;
        const end = query.end;
        const spiderName = params.spiderName;
        let sql = 'SELECT * from project.project where 1 = 1 ';
        if(spiderName !== 'all') {
            sql += ` and lower(site) = lower('${spiderName}')`;
        }
        if(start) {
            sql += ` and updated_time >= to_date('${start}', 'DD-MM-YYYY')`;
        }
        if(end) {
            sql += ` and updated_time <= to_date('${end}', 'DD-MM-YYYY') + 1`;
        }

        console.log(sql);

        pool.query(sql)
            .then(res => {
                callback(res.rows);
            });
    }
};